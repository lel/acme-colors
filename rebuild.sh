#!/usr/bin/env sh

if [ -z "$PLAN9" ]; then
  echo "\$PLAN9 is not set. Set the \$PLAN9 environmental variable to location of plan9port." >&2
  exit 1
fi

cd $PLAN9/src/cmd/acme

9 mk install
