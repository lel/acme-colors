#!/usr/bin/env sh

if [ -z "$PLAN9" ]; then
  echo "\$PLAN9 is not set. Set the \$PLAN9 environmental variable to location of plan9port." >&2
  exit 1
fi

script=$(readlink -f "$0")
loc=$(dirname "$script")

if [ ! -d $PLAN9/src/cmd/acme ] ; then
  echo "Couldn't open $PLAN9/src/cmd/acme!" >&2
  exit 1
fi

cd "$PLAN9/src/cmd/acme"

rm acme.c
rm config.h
rm mkfile

echo $loc

ln -s "$loc/acme.c" ./acme.c
ln -s "$loc/mkfile" ./mkfile
ln -s "$loc/config.h" ./config.h

echo "Unless something horrible just happened (would probably have shown up in the output), you should be all set. Don't forget to change config.h to the colors you want!"
